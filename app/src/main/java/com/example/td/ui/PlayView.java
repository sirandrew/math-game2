package com.example.td.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.td.R;

import static com.example.td.ui.Difficulty.diff;
import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.Scorrect;
import static com.example.td.ui.MenuActivity.SAllAnswers;
import static com.example.td.ui.MenuActivity.Spoints;
import static com.example.td.ui.MenuActivity.correct;
import static com.example.td.ui.MenuActivity.allanswers;
import static com.example.td.ui.MenuActivity.points;

/**
 * Created by Андрей on 29.03.2017.
 */

public class PlayView extends AppCompatActivity implements View.OnClickListener {

    TextView task;
    EditText answer;
    Button enter;
    String playtask;
    static int result;
    String playanswer;
    TextView score;
    FrameLayout background;



    private void layout() {
        int a [] = GameView.generator();
        result = a [0] ;
        int number1 = a[5];
        int number2 = a[6];
        int sign = a [7];

        if (sign == 0) {
           playtask = Integer.toString(number1) + " + " + Integer.toString(number2) + " = ?" ;
        }
        else {
            if (number1>number2){
                playtask = Integer.toString(number1) + " - " + Integer.toString(number2)+" = ?" ;
            }
            else {
                playtask = Integer.toString(number2) + " - " + Integer.toString(number1)+" = ?" ;
            }


    }
        task.setText(playtask);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.play_activity);
        background = (FrameLayout) findViewById(R.id.play_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }

        task = (TextView) findViewById(R.id.play_task);
        answer = (EditText) findViewById(R.id.play_answer);
        score = (TextView) findViewById(R.id.play_score);
        enter = (Button)  findViewById(R.id.enter);
        playtask = getString (R.string.play_task);
        playanswer = getString(R.string.answer);
        enter.setOnClickListener(this);
        score.setText("Cчет: " + points);

        layout();




    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view){


        if (answer.getText().toString().equals(Integer.toString(result))){
            answer.setText("");

            points = points + diff+2;
            correct +=1;
            allanswers +=1;
            POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
            SharedPreferences.Editor ed = POINTS.edit();
            ed.putInt(Spoints, points);
            ed.apply();
            ed.putInt(Scorrect,correct);
            ed.apply();
            ed.putInt(SAllAnswers, allanswers);
            ed.apply();
            score.setText("Cчет: " + points);
            Toast toast = Toast.makeText(getApplicationContext(),"Правильно!", Toast.LENGTH_SHORT);
            toast.show();
            layout();
        }
        else {
            if(points-diff+2 > 0){
                answer.setText("");
                points = points-diff+2;
                allanswers +=1;
                POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
                SharedPreferences.Editor ed = POINTS.edit();
                ed.putInt(Spoints, points);
                ed.apply();
                ed.putInt(SAllAnswers, allanswers);
                ed.apply();

                score.setText("Cчет: " + points);
                Toast toast = Toast.makeText(getApplicationContext(),"Неправильно :(", Toast.LENGTH_SHORT);
                toast.show();
                layout();

            }
            else {
                answer.setText("");
                points = 0;
                allanswers +=1;
                POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
                SharedPreferences.Editor ed = POINTS.edit();
                ed.putInt(Spoints, points);
                ed.apply();
                ed.putInt(SAllAnswers, allanswers);
                ed.apply();
                score.setText("Cчет: " + points);
                Toast toast = Toast.makeText(getApplicationContext(),"Неправильно :(", Toast.LENGTH_SHORT);
                toast.show();
                layout();
            }

        }

        }

}
