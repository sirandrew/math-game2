package com.example.td.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.td.R;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;

/**
 * Created by Андрей on 04.05.2017.
 */

public class GameMode extends AppCompatActivity implements View.OnClickListener {
    static int mode;
    Button test_button;
    Button control_button;
    Button multi_button;
    RelativeLayout background;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.game_mode);
        background = (RelativeLayout) findViewById(R.id.gamemode_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
        test_button = (Button) findViewById(R.id.test_button);
        control_button = (Button) findViewById(R.id.control_button);
        multi_button = (Button) findViewById(R.id.multiplication_table);
        test_button.setOnClickListener(this);
        control_button.setOnClickListener(this);
        multi_button.setOnClickListener(this);

    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.test_button:
                Intent diff = new Intent(GameMode.this, Difficulty.class);
                startActivity(diff);
                mode =1;
                break;
            case R.id.control_button:
                Intent diff1 = new Intent(GameMode.this, Difficulty.class);
                startActivity(diff1);
                mode =2;
                break;
            case R.id.multiplication_table:
                Intent multi = new Intent(GameMode.this, MultiplicationTable.class);
                startActivity(multi);


        }
    }
}