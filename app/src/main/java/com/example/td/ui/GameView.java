package com.example.td.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.td.R;

import static com.example.td.ui.Difficulty.diff;
import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.Scorrect;
import static com.example.td.ui.MenuActivity.SAllAnswers;
import static com.example.td.ui.MenuActivity.Spoints;
import static com.example.td.ui.MenuActivity.correct;
import static com.example.td.ui.MenuActivity.allanswers;
import static com.example.td.ui.MenuActivity.points;


/**
 * Created by Андрей on 07.02.2017.
 */

public class GameView extends AppCompatActivity implements View.OnClickListener {

    TextView text;
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    String task;
    static int spot;
    TextView score;
    RelativeLayout background;








    public static int [] generator (){

        int sign;
        int number1;
        int number2;
        int result;
        int spot;
        int answer2;
        int answer3;
        int answer4;


        sign = (int) (Math.random()*2);
        number1 = (int) (Math.random()*10*diff+1);
        number2 = (int) (Math.random()*10*diff+1);

        if (sign == 0){
            result= number1 + number2;
        }
        else {
            if (number1>number2){
                result= number1-number2;
            }
            else {
                result=number2-number1;
            }
        }
        spot = (int) (Math.random()*4+1);
        if (sign==0) {
            answer2 = (int) ((result/2 + Math.random() * diff * 5) + (result/2 - Math.random() * diff * 5));
            answer3 = (int) ((result/2 + Math.random() * diff * 5) + (result/2 - Math.random() * diff * 5));
            answer4 = (int) ((result/2 + Math.random() * diff * 5) + (result/2 - Math.random() * diff * 5));

            while (result == answer2 || result == answer3 || result == answer4 || answer2 == answer3 || answer2 == answer4 || answer3 == answer4) {
                answer2 = (int) ((result/2 + Math.random() * diff *5 ) + (result/2 - Math.random() * diff * 5));
                answer3 = (int) ((result/2 + Math.random() * diff * 5) + (result/2 - Math.random() * diff *5));
                answer4 = (int) ((result/2 + Math.random() * diff *5 ) + (result/2 - Math.random() * diff * 5));
            }
        }
        else {
            answer2 = (int) ((result + Math.random() * diff * 3) - (result - Math.random() * diff * 3));
            answer3 = (int) ((result + Math.random() * diff * 3) - (result - Math.random() * diff * 3));
            answer4 = (int) ((result + Math.random() * diff * 3) - (result - Math.random() * diff * 3));

            while (result == answer2 || result == answer3 || result == answer4 || answer2 == answer3 || answer2 == answer4 || answer3 == answer4) {
                answer2 = (int) ((result + Math.random() * diff * 3) - (result - Math.random() * diff * 3));
                answer3 = (int) ((result + Math.random() * diff * 3) - (result - Math.random() * diff * 3));
                answer4 = (int) ((result + Math.random() * diff * 3) - (result - Math.random() * diff * 3));
            }
        }

        int array [] ={result,spot,answer2,answer3,answer4, number1,number2, sign};

        return array;

    }


    public  void save (){

        POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
        SharedPreferences.Editor ed = POINTS.edit();

        ed.putInt(Spoints, points );
        ed.apply ();
        ed.putInt(Scorrect, correct);
        ed.apply();
        ed.putInt(SAllAnswers, allanswers);
        ed.apply();
    }

    protected int layout (){

        int a [] = generator();
        int result = a[0];
        spot = a[1];
        int answer2= a[2];
        int answer3= a[3];
        int answer4= a[4];
        int number1 = a[5];
        int number2 = a[6];
        int sign = a [7];

        if (sign == 0) {
            task = Integer.toString(number1) + " + " + Integer.toString(number2) + " = ?" ;
        }
        else {
            if (number1>number2){
                task = Integer.toString(number1) + " - " + Integer.toString(number2)+" = ?" ;
            }
            else {
                task = Integer.toString(number2) + " - " + Integer.toString(number1)+" = ?" ;
            }

        }
        text.setText(task);

        switch (spot){
            case 1: b1.setText(Integer.toString(result));
                b2.setText(Integer.toString(answer2));
                b3.setText(Integer.toString(answer3));
                b4.setText(Integer.toString(answer4));
                break;
            case 2: b1.setText(Integer.toString(answer2));
                b2.setText(Integer.toString(result));
                b3.setText(Integer.toString(answer3));
                b4.setText(Integer.toString(answer4));
                break;
            case 3: b1.setText(Integer.toString(answer2));
                b2.setText(Integer.toString(answer3));
                b3.setText(Integer.toString(result));
                b4.setText(Integer.toString(answer4));
                break;
            case 4: b1.setText(Integer.toString(answer2));
                b2.setText(Integer.toString(answer3));
                b3.setText(Integer.toString(answer4));
                b4.setText(Integer.toString(result));
                break;
        }
        return spot;
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.game_activity);
        background = (RelativeLayout) findViewById(R.id.game_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }

        text = (TextView) findViewById(R.id.text);
        score = (TextView) findViewById(R.id.game_score);
        b1 = (Button) findViewById(R.id.answer1);
        b2 = (Button) findViewById(R.id.answer2);
        b3 = (Button) findViewById(R.id.answer3);
        b4 = (Button) findViewById(R.id.answer4);
        task = getString (R.string.task);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        score.setText("Cчет: " + points);

        layout();

    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.answer1:
                if (spot == 1) {
                    layout();
                    points +=diff;
                    correct +=1;
                    allanswers +=1;
                    Toast toast = Toast.makeText(getApplicationContext(),"Правильно!", Toast.LENGTH_SHORT);
                    toast.show();
                    save();
                    score.setText("Cчет: " + points);
                }
                else {
                    layout();
                    if (points-diff >0) {
                        points -= diff;
                        allanswers +=1;
                    }
                    else {
                        points=0;
                        allanswers +=1;
                    }
                    Toast toast = Toast.makeText(getApplicationContext(),"Неправильно:(", Toast.LENGTH_SHORT);
                    toast.show();
                    save();
                    score.setText("Cчет: " + points);
                }
                break;

            case R.id.answer2:
                if (spot == 2) {

                    layout();
                    points +=diff;
                    allanswers+=1;
                    Toast toast = Toast.makeText(getApplicationContext(),"Правильно!", Toast.LENGTH_SHORT);
                    toast.show();
                    correct +=1;
                    save();
                    score.setText("Cчет: " + points);
                }
                else {
                        layout();
                    if (points-diff >0) {
                        points -= diff;
                        allanswers +=1;
                    }
                    else {
                        points=0;
                        allanswers +=1;
                    }
                        Toast toast = Toast.makeText(getApplicationContext(),"Неправильно:(", Toast.LENGTH_SHORT);
                        toast.show();
                        save();
                        score.setText("Cчет: " + points);
                    }
                break;

            case R.id.answer3:
                if (spot == 3) {

                    layout();
                    points +=diff;
                    correct +=1;
                    allanswers+=1;
                    Toast toast = Toast.makeText(getApplicationContext(),"Правильно!", Toast.LENGTH_SHORT);
                    toast.show();
                    save();
                    score.setText("Cчет: " + points);
                }
                else {
                    layout();
                    if (points-diff >0) {
                        points -= diff;
                        allanswers +=1;
                    }
                    else {
                        points=0;
                        allanswers +=1;
                    }
                    Toast toast = Toast.makeText(getApplicationContext(),"Неправильно:(", Toast.LENGTH_SHORT);
                    toast.show();
                    save();
                    score.setText("Cчет: " + points);
                }
                break;

            case R.id.answer4:
                if (spot == 4) {

                    layout();
                    points +=diff;
                    allanswers+=1;
                    Toast toast = Toast.makeText(getApplicationContext(),"Правильно!", Toast.LENGTH_SHORT);
                    correct +=1;
                    toast.show();
                    save();
                    score.setText("Cчет: " + points);
                }
                else {
                layout();
                if (points-diff >0) {
                    points -= diff;
                    allanswers +=1;
                }
                    else {
                    points=0;
                    allanswers +=1;
                }

                Toast toast = Toast.makeText(getApplicationContext(),"Неправильно:(", Toast.LENGTH_SHORT);
                toast.show();
                save();
                score.setText("Cчет: " + points);
            }
                break;

        }
}}