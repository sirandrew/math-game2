package com.example.td.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.td.R;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.Spoints;
import static com.example.td.ui.MenuActivity.points;

/**
 * Created by Андрей on 12.05.2017.
 */

public class BackgroundShop extends AppCompatActivity implements View.OnClickListener {


    ImageButton grey;
    ImageButton red;
    ImageButton blue;
    RelativeLayout background;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.background_shop_activity);
        grey = (ImageButton) findViewById(R.id.backgroundgrey);
        red = (ImageButton) findViewById(R.id.backgroundred);
        blue = (ImageButton) findViewById(R.id.backgroundblue);
        grey.setOnClickListener(this);
        red.setOnClickListener(this);
        blue.setOnClickListener(this);
        background = (RelativeLayout) findViewById(R.id.background_shop_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }


    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backgroundgrey:
                /*if (points > 100){
                points -=100;*/
                BackgroundColor = 0;
                POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
                SharedPreferences.Editor ed = POINTS.edit();
                ed.putInt(SBackgroundColor, BackgroundColor);
                ed.apply();
                Intent i = new Intent(BackgroundShop.this, MenuActivity.class);
                startActivity(i);
                /*}
                else{
                    Toast toast = Toast.makeText(getApplicationContext(),"Недостаточно очков", Toast.LENGTH_SHORT);
                    toast.show();
                }*/
                break;
            case R.id.backgroundred:
                /*if (points > 100) {
                    points -=100;*/
                    BackgroundColor = 1;
                    POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = POINTS.edit();
                    ed1.putInt(SBackgroundColor, BackgroundColor);
                    ed1.apply();
                    Intent q = new Intent(BackgroundShop.this, MenuActivity.class);
                    startActivity(q);
                /*}
                else {
                    Toast toast = Toast.makeText(getApplicationContext(),"Недостаточно очков", Toast.LENGTH_SHORT);
                    toast.show();
                }*/
                break;

            case R.id.backgroundblue:
                /*if (points>100){
                points -=100;*/
                BackgroundColor = 2;
                POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
                SharedPreferences.Editor ed2 = POINTS.edit();
                ed2.putInt(SBackgroundColor, BackgroundColor);
                ed2.apply();
                Intent w = new Intent(BackgroundShop.this, MenuActivity.class);
                startActivity(w);
                /*}
                else {
                    Toast toast = Toast.makeText(getApplicationContext(),"Недостаточно очков", Toast.LENGTH_SHORT);
                    toast.show();
                }*/
                break;

        }

    }
}
