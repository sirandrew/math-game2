package com.example.td.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.td.R;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.Scorrect;
import static com.example.td.ui.MenuActivity.SAllAnswers;
import static com.example.td.ui.MenuActivity.Spoints;
import static com.example.td.ui.MenuActivity.correct;
import static com.example.td.ui.MenuActivity.allanswers;
import static com.example.td.ui.MenuActivity.points;

/**
 * Created by Андрей on 13.05.2017.
 */

public class MultiplicationTable extends AppCompatActivity implements View.OnClickListener {

    TextView Task;
    EditText Answer;
    Button enter;
    TextView score;
    FrameLayout background;
    String multitask;
    String multianswer;
    static int result;

    private void layout() {
        int a = (int) (Math.random() * 8 + 2);
        int b = (int) (Math.random() * 8 + 2);
        result = a * b;
        multitask = Integer.toString(a) + " * " + Integer.toString(b) + " = ?";
        Task.setText(multitask);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.multiplication_table);
        background = (FrameLayout) findViewById(R.id.multi_background);
        switch (BackgroundColor) {
            case 0:
                background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:
                background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:
                background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
        Task = (TextView) findViewById(R.id.multi_task);
        Answer = (EditText) findViewById(R.id.multi_answer);
        score = (TextView) findViewById(R.id.multi_score);
        enter = (Button) findViewById(R.id.multi_enter);
        enter.setOnClickListener(this);
        score.setText("Cчет: " + points);
        layout();
    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        if (Answer.getText().toString().equals(Integer.toString(result))) {
            Answer.setText("");

            points +=4;
            correct +=1;
            allanswers +=1;
            POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
            SharedPreferences.Editor ed = POINTS.edit();
            ed.putInt(Spoints, points);
            ed.apply();
            ed.putInt(Scorrect,correct);
            ed.apply();
            ed.putInt(SAllAnswers, allanswers);
            ed.apply();
            score.setText("Cчет: " + points);
            Toast toast = Toast.makeText(getApplicationContext(),"Правильно!", Toast.LENGTH_SHORT);
            toast.show();
            layout();
        }
        else if (points-4>0){
            Answer.setText("");
            points -=4;
            allanswers +=1;
            POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
            SharedPreferences.Editor ed = POINTS.edit();
            ed.putInt(Spoints, points);
            ed.apply();
            ed.putInt(SAllAnswers, allanswers);
            ed.apply();

            score.setText("Cчет: " + points);
            Toast toast = Toast.makeText(getApplicationContext(),"Неправильно :(", Toast.LENGTH_SHORT);
            toast.show();
            layout();
        }
        else {
            Answer.setText("");
            points = 0;
            allanswers +=1;
            POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
            SharedPreferences.Editor ed = POINTS.edit();
            ed.putInt(Spoints, points);
            ed.apply();
            ed.putInt(SAllAnswers, allanswers);
            ed.apply();

            score.setText("Cчет: " + points);
            Toast toast = Toast.makeText(getApplicationContext(),"Неправильно :(", Toast.LENGTH_SHORT);
            toast.show();
            layout();
        }
    }
}