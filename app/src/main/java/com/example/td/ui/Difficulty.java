package com.example.td.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.td.R;

import static com.example.td.ui.GameMode.mode;
import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;


/**
 * Created by Андрей on 07.02.2017.
 */

public class Difficulty extends AppCompatActivity implements View.OnClickListener {

    public static int diff;
    Button easy;
    Button medium;
    Button hard;
    RelativeLayout background;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.difficuty_activity);
        background = (RelativeLayout) findViewById(R.id.diff_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }

        easy = (Button) findViewById(R.id.easy);
        medium = (Button) findViewById(R.id.medium);
        hard = (Button) findViewById(R.id.hard);
        easy.setOnClickListener(this);
        medium.setOnClickListener(this);
        hard.setOnClickListener(this);




    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.easy:
                if (mode ==1){
                    Intent startEasy = new Intent(this, GameView.class);
                    diff = 1;
                    startActivity(startEasy);
                }
                else {
                    Intent startEasy = new Intent(this, PlayView.class);
                    diff = 1;
                    startActivity(startEasy);
                }
                break;
            case R.id.medium:
                if (mode ==1){
                    Intent startMedium = new Intent(this, GameView.class);
                    diff =3;
                    startActivity(startMedium);
                }
                else {
                    Intent startMedium = new Intent(this, PlayView.class);
                    diff =3;
                    startActivity(startMedium);
                }
                break;
            case R.id.hard:
                if (mode ==1){
                    Intent startHard = new Intent(this, GameView.class);
                    diff =5;
                    startActivity(startHard);
                }
                else {
                    Intent startHard = new Intent(this, PlayView.class);
                    diff =5;
                    startActivity(startHard);
                }

                break;
        }
    }
}