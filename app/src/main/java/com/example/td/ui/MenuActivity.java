package com.example.td.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.td.R;



public class MenuActivity extends AppCompatActivity implements View.OnClickListener {


    Button test;
    Button play;
    Button multi;
    ImageButton settings;
    ImageButton info;
    static SharedPreferences POINTS;
    final static String Spoints = "Spoints";
    final static String SAllAnswers = "SAllAnswers";
    final static String Scorrect = "Scorrect";
    final static String SBackgroundColor = "SBackground";
    public static int BackgroundColor;
    public static int correct;
    public static int allanswers;
    public static int points;
    RelativeLayout background;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu);
        test = (Button) findViewById(R.id.play_button);
        play = (Button) findViewById(R.id.profile_button);
        multi = (Button) findViewById(R.id.shop_button);
        settings = (ImageButton) findViewById(R.id.settings);
        info = (ImageButton) findViewById(R.id.info);
        test.setOnClickListener(this);
        play.setOnClickListener(this);
        multi.setOnClickListener(this);
        settings.setOnClickListener(this);
        info.setOnClickListener(this);
        background = (RelativeLayout) findViewById(R.id.activity_menu);

        POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
        points = POINTS.getInt(Spoints,0);
        correct = POINTS.getInt(Scorrect,0);
        allanswers = POINTS.getInt(SAllAnswers,0);
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);

        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }

    }
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue   );
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.play_button:
                Intent mode = new Intent(MenuActivity.this, GameMode.class);
                startActivity(mode);
                break;
            case R.id.profile_button:
                Intent profile = new Intent(MenuActivity.this, Profile.class);
                startActivity(profile);
                break;
            case R.id.shop_button:
                Intent shop = new Intent((MenuActivity.this), Shop.class);
                startActivity(shop);
                break;
            case R.id.settings:
                Intent settings = new Intent(this, Settings.class);
                startActivity(settings);
                break;
            case R.id.info:
                Intent info = new Intent(this, Info.class);
                startActivity(info);
                break;
        }
    }
}
