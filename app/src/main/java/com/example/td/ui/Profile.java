package com.example.td.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.td.R;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.correct;
import static com.example.td.ui.MenuActivity.allanswers;

/**
 * Created by Андрей on 12.05.2017.
 */

public class Profile extends AppCompatActivity implements View.OnClickListener {
    TextView Correct;
    TextView Winrate;
    String Swinrate;
    float winrate;
    RelativeLayout background;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.profile_activity);
        Correct = (TextView) findViewById(R.id.correct);
        Winrate = (TextView) findViewById(R.id.winrate);
        background = (RelativeLayout) findViewById(R.id.profile_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }

        if (allanswers==0){
            winrate=0;
        }
        else {
            winrate = correct*100/allanswers ;
        }
        Swinrate = getString(R.string.winrate);
        String formatedWin = String.format("%.0f",winrate);
        Correct.setText("Правильных ответов: " + Integer.toString(correct));
        Winrate.setText(Swinrate+ formatedWin + "%");

}
    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {

    }
}