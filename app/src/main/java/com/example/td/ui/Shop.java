package com.example.td.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.td.R;

import static com.example.td.ui.MenuActivity.BackgroundColor;
import static com.example.td.ui.MenuActivity.POINTS;
import static com.example.td.ui.MenuActivity.SBackgroundColor;
import static com.example.td.ui.MenuActivity.points;

/**
 * Created by Андрей on 11.04.2017.
 */

public class Shop extends AppCompatActivity implements View.OnClickListener {

    TextView score;
    Button Background;
    Button buttons;
    RelativeLayout background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.shop_activity);
        background = (RelativeLayout) findViewById(R.id.shop_background);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
        score = (TextView) findViewById(R.id.shop_score);
        score.setText("Cчет: " + points);
        Background =(Button) findViewById(R.id.background_shop);
        buttons= (Button) findViewById(R.id.buttons_shop);
        Background.setOnClickListener(this);
        buttons.setOnClickListener(this);

    }

    @Override
    public void onStart(){
        super.onStart();
        BackgroundColor = POINTS.getInt(SBackgroundColor,0);
        switch (BackgroundColor) {
            case 0:background.setBackgroundResource(R.drawable.backgroundgrey);
                break;
            case 1:background.setBackgroundResource(R.drawable.backgroundred);
                break;
            case 2:background.setBackgroundResource(R.drawable.backgroundblue);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.background_shop:
                Intent backgroundshop = new Intent(Shop.this, BackgroundShop.class);
                startActivity(backgroundshop);
                break;
            case R.id.buttons_shop:
                break;

        }
    }
}
